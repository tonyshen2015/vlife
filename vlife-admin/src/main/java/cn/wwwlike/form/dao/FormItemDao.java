package cn.wwwlike.form.dao;

import cn.wwwlike.form.entity.FormItem;
import cn.wwwlike.vlife.core.dsl.DslDao;
import org.springframework.stereotype.Repository;

@Repository
public class FormItemDao extends DslDao<FormItem> {
}
