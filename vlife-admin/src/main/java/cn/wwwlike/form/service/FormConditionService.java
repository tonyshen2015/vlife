package cn.wwwlike.form.service;

import cn.wwwlike.form.dao.FormConditionDao;
import cn.wwwlike.form.entity.FormCondition;
import cn.wwwlike.vlife.core.VLifeService;
import org.springframework.stereotype.Service;

@Service
public class FormConditionService extends VLifeService<FormCondition, FormConditionDao> {
}
