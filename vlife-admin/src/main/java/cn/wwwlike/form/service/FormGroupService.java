package cn.wwwlike.form.service;

import cn.wwwlike.form.dao.FormGroupDao;
import cn.wwwlike.form.entity.FormGroup;
import cn.wwwlike.vlife.core.VLifeService;
import org.springframework.stereotype.Service;

@Service
public class FormGroupService extends VLifeService<FormGroup, FormGroupDao> {
}
